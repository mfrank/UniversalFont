# Universal Font

© Maximilian Josef Frank  
*Created with [glyphrstudio.com/online](https://glyphrstudio.com/online)*

Universal (as in *intergalactic* not in *unicode*) font consisting only of very simple and basic geometric shapes. It contains all basic IPA sounds as letters or ligatures.